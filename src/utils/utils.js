const SALUDO = 'saludo';
const TIPO_CUENTA_AHORRO = "CAHO";
const NUMERO_CUENTA_SIZE = 16;
const PATH_PAGE_LOGIN = "";
const PATH_PAGE_MIS_CUENTAS = 'mis-cuentas';
const PATH_PAGE_MIS_TRANSFERENCIAS = 'mis-transferencias';
const URL_BACKEND = (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1') ? 'http=//127.0.0.1=3000/bxip-back/v1' : '...';

function getSimboloMoneda(monedas, codigo) {
    let descripcion = '';
    monedas.forEach(item => {
        if (item.id == codigo)
            descripcion = item.valor;
    })
    return descripcion;
}

function irAPagina(pagina) {
    window.history.pushState({}, null, '/bxip-front' + (pagina ? "/" + pagina : ""));
    window.dispatchEvent(new CustomEvent('location-changed'));
}